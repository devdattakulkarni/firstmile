# README #

FirstMile is a cloud developer's sandbox. It supports building and deploying Python Flask web applications to target clouds (Google and Amazon). FirstMile provides a Docker-based local sandbox that can be used locally while developing applications. Moreover, FirstMile supports provisioning of services such as MySQL, Google Cloud SQL, Amazon RDS in a cloud agnostic manner.

### How do I set up FirstMile? 

We are releasing 1.0.0 version of FirstMile here: https://www.dropbox.com/s/dgbx822i7h40kbp/firstmile-1.0.0.tar.gz?dl=0

FirstMile has been tested on following platforms: Ubuntu 14.04, Ubuntu 16.04, Mac OS X (El Capitan, Darwin 15.4.0)


You can download the tarball and set up FirstMile as follows:

wget https://www.dropbox.com/s/dgbx822i7h40kbp/firstmile-1.0.0.tar.gz?dl=0 -O firstmile-1.0.0.tar.gz

gunzip firstmile-1.0.0.tar.gz

tar -xvf firstmile-1.0.0.tar

cd firstmile-1.0.0/firstmile

There is a README in 'firstmile' folder which outlines setup steps that are required for setting up FirstMile with AWS and Google. You will find sample programs as well in the tarball.

### Issues / Feedback

Please submit any suggestions, feedback, bug reports [here](https://bitbucket.org/devdattakulkarni/firstmile/issues?status=new&status=open).

### Contact ###

Devdatta Kulkarni (devdattakulkarni at gmail dot com)